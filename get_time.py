import requests
from uuid import uuid4
from datetime import datetime
import json
import logging

logger = logging.getLogger(__name__)

TRAINLINE_HOME = 'https://www.trainline.fr/'
TRAINLINE_API = 'https://www.trainline.fr/api/v5_1/search'

HEADERS = {
            'Accept': 'application/json',
            'User-Agent': 'CaptainTrain/1574360965(web) (Ember 3.5.1)',
            'Accept-Language': 'fr',
            'Content-Type': 'application/json; charset=UTF-8',
            'Host': 'www.trainline.eu',
        }

def get_station_id(city):
    f = open('./stations_mini.csv', 'r')
    lines = f.readlines()

    ids = []
    for line in lines:
        station_id, city_csv = line.split(';')
        if city.lower() in city_csv.lower():
            ids.append((station_id, city_csv))

    return sorted(ids, key=lambda x: len(x[1]))[0][0]


def get_faster_trains(city_from, city_to, departure_date):
    logger.debug(f'Gettings trains for {city_from} -> {city_to}')

    station_from = get_station_id(city_from)
    station_to = get_station_id(city_to)
    passenger =  [{"id": str(uuid4()),
                   "label": "adult",
                   "age": 26,
                   "cards":[],
                   "cui": None}]
    data = {
            "search": {
                "arrival_station_id": station_to,
                "departure_date": departure_date,
                "departure_station_id": station_from,
                "exchangeable_part": None, 
                "exchangeable_pnr_id": None,
                "is_next_available": False,
                "is_previous_available": False,
                "passengers": passenger,
                "source": None,
                "via_station_id": None,
                "systems": [
                    "sncf",
                    "db",
                    "idtgv",
                    "ouigo",
                    "trenitalia",
                    "ntv",
                    "hkx",
                    "renfe",
                    "cff",
                    "benerail",
                    "ocebo",
                    "westbahn",
                    "leoexpress",
                    "locomore",
                    "busbud",
                    "flixbus",
                    "distribusion",
                    "cityairporttrain",
                    "obb",
                    "timetable"
                ]
            }
        }
    
    s = requests.Session()
    s.get('https://www.trainline.fr/search/geneve/baden-baden/2020-05-29-18:00/2020-05-31-18:00')
    r = s.post(TRAINLINE_API, headers=HEADERS, data=json.dumps(data))
    
    trips = r.json()['trips']
    filtered = []
    for trip in trips:
        if trip['cents'] != 0:  # means the train isn't full
            logging.debug(trip)
            departure = datetime.strptime(trip['departure_date'], '%Y-%m-%dT%H:%M:%S%z')
            arrival = datetime.strptime(trip['arrival_date'], '%Y-%m-%dT%H:%M:%S%z')
            travel_time = arrival - departure
            price = trip['cents'] / 100
            
            filtered.append((travel_time, price))

    trains = sorted(filtered, key=lambda x: x[0])
    logger.debug(trains)

    if len(trains) > 0:
        return trains[0]
    return None, None


if __name__ == "__main__":
    logger.setLevel(logging.DEBUG)

    departure_date = '2020-05-29T18:00:00UTC'

    cities_from = ['Genève', 'Paris']
    cities_to = ['Baden-Baden', 'Toulouse', 'Avignon', 'Kraków', 'Dresden', 
                 'Paris', 'Genève']

    for city_from in cities_from:
        for city_to in cities_to:
            if city_to == city_from:
                continue

            travel_time, price = get_faster_trains(city_from, city_to, departure_date)
            print(f'From {city_from} to {city_to}')
            print(f"    Travel time: {travel_time}")
            print(f"    Price: {price}")
